let rsa = require("node-rsa");
let fs = require("fs");
const NodeRSA = require("node-rsa");

function GeneratePair(){
    let key = new rsa().generateKeyPair();

    let publicKey = key.exportKey("public");

    let privateKey = key.exportKey("private");

    fs.openSync("./Keys/public.pem", "w");
    fs.writeFileSync("./Keys/public.pem", publicKey, "utf8");

    fs.openSync("./Keys/private.pem", "w");
    fs.writeFileSync("./Keys/private.pem", privateKey, "utf8");
}

GeneratePair();

